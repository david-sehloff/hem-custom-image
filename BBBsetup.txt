System changes made to testing Beaglebone (4.4.30-ti-r64)
6/27/17
apt-get update
apt-get upgrade
apt-get install libgtk-3-dev
//Add the overlays for SPI0 for display and I2C1 for touch:
nano /boot/uEnv.txt
//Add the line
cape_enable=bone_capemgr.enable_partno=BB-SPIDEV0,BB-I2C1-Touch,BB-UART4
//Uncomment the line
dtb=am335x-boneblack-emmc-overlay.dtb

//Add touch driver: paste the file ft6236.ko to Beaglebone
//Move to extra module directory:
mv ft6236.ko /lib/modules/$(uname -r)/extra/
//Paste the I2C touch device tree file BB-I2C1-Touch.dts, compile and move to /lib/firmware:
dtc -O dtb -o BB-I2C1-Touch-00A0.dtbo -b 0 -@ BB-I2C1-Touch.dts
mv BB-I2C1-Touch-00A0.dtbo /lib/firmware
//Include the name in /etc/default/capemgr:
nano /etc/default/capemgr
//Change the last line to
CAPE=BB-I2C1-Touch
//uninstall lightdm
apt-get remove lightdm
apt-get autoremove
//install ssl dev files
apt-get install libssl-dev
//clone hemapp repo
git clone https://gitlab.com/microgridenergymanager/hemapp.git


//Compiling and running hemapp
cd hemapp
make clean
make
export export LD_LIBRARY_PATH=.
./hemapp


System changes made to testing Beaglebone (07-14-17 console image)
7/31/17
apt-get update
apt-get upgrade
apt-get install libgtk-3-dev
OUTPUT:

The following additional packages will be installed:
  adwaita-icon-theme at-spi2-core autoconf automake autopoint autotools-dev
  dconf-gsettings-backend dconf-service debhelper dh-autoreconf
  dh-strip-nondeterminism fontconfig fontconfig-config fonts-dejavu-core
  gettext gettext-base gir1.2-atk-1.0 gir1.2-atspi-2.0 gir1.2-freedesktop
  gir1.2-gdkpixbuf-2.0 gir1.2-glib-2.0 gir1.2-gtk-3.0 gir1.2-pango-1.0
  glib-networking glib-networking-common glib-networking-services groff-base
  gsettings-desktop-schemas gtk-update-icon-cache hicolor-icon-theme
  icu-devtools intltool-debian libarchive-zip-perl libatk-bridge2.0-0
  libatk-bridge2.0-dev libatk1.0-0 libatk1.0-data libatk1.0-dev libatspi2.0-0
  libatspi2.0-dev libcairo-gobject2 libcairo-script-interpreter2 libcairo2
  libcairo2-dev libcolord2 libcroco3 libcups2 libdatrie1 libdbus-1-dev
  libdconf1 libdrm-amdgpu1 libdrm-dev libdrm-etnaviv1 libdrm-exynos1
  libdrm-freedreno1 libdrm-nouveau2 libdrm-omap1 libdrm-radeon1 libdrm-tegra0
  libdrm2 libegl1-mesa libegl1-mesa-dev libepoxy-dev libepoxy0 libexpat1-dev
  libfile-stripnondeterminism-perl libfontconfig1 libfontconfig1-dev
  libfreetype6 libfreetype6-dev libgbm1 libgdk-pixbuf2.0-0
  libgdk-pixbuf2.0-common libgdk-pixbuf2.0-dev libgirepository-1.0-1
  libglib2.0-bin libglib2.0-data libglib2.0-dev libgraphite2-3
  libgraphite2-dev libgtk-3-0 libgtk-3-bin libgtk-3-common libharfbuzz-dev
  libharfbuzz-gobject0 libharfbuzz-icu0 libharfbuzz0b libice-dev libice6
  libicu-dev libjbig0 libjpeg62-turbo libjson-glib-1.0-0
  libjson-glib-1.0-common liblcms2-2 libltdl-dev libltdl7 liblzo2-2
  libmail-sendmail-perl libpango-1.0-0 libpango1.0-dev libpangocairo-1.0-0
  libpangoft2-1.0-0 libpangoxft-1.0-0 libpcre16-3 libpcre3-dev libpcre32-3
  libpcrecpp0v5 libpixman-1-0 libpixman-1-dev libpng-dev libpng-tools
  libpng16-16 libproxy1v5 libpthread-stubs0-dev librest-0.7-0 librsvg2-2
  librsvg2-common libsm-dev libsm6 libsoup-gnome2.4-1 libsoup2.4-1
  libsys-hostname-long-perl libthai-data libthai0 libtiff5 libtimedate-perl
  libtool libwayland-bin libwayland-client0 libwayland-cursor0 libwayland-dev
  libwayland-egl1-mesa libwayland-server0 libx11-6 libx11-data libx11-dev
  libx11-doc libx11-xcb-dev libx11-xcb1 libxau-dev libxau6 libxcb-dri2-0
  libxcb-dri2-0-dev libxcb-dri3-0 libxcb-dri3-dev libxcb-glx0 libxcb-glx0-dev
  libxcb-present-dev libxcb-present0 libxcb-randr0 libxcb-randr0-dev
  libxcb-render0 libxcb-render0-dev libxcb-shape0 libxcb-shape0-dev
  libxcb-shm0 libxcb-shm0-dev libxcb-sync-dev libxcb-sync1 libxcb-xfixes0
  libxcb-xfixes0-dev libxcb1 libxcb1-dev libxcomposite-dev libxcomposite1
  libxcursor-dev libxcursor1 libxdamage-dev libxdamage1 libxdmcp-dev libxdmcp6
  libxext-dev libxext6 libxfixes-dev libxfixes3 libxft-dev libxft2 libxi-dev
  libxi6 libxinerama-dev libxinerama1 libxkbcommon-dev libxkbcommon0
  libxrandr-dev libxrandr2 libxrender-dev libxrender1 libxshmfence-dev
  libxshmfence1 libxtst-dev libxtst6 libxxf86vm-dev libxxf86vm1 man-db
  pkg-config po-debconf shared-mime-info wayland-protocols x11-common
  x11proto-composite-dev x11proto-core-dev x11proto-damage-dev
  x11proto-dri2-dev x11proto-fixes-dev x11proto-gl-dev x11proto-input-dev
  x11proto-kb-dev x11proto-randr-dev x11proto-record-dev x11proto-render-dev
  x11proto-xext-dev x11proto-xf86vidmode-dev x11proto-xinerama-dev xkb-data
  xorg-sgml-doctools xtrans-dev zlib1g-dev
Suggested packages:
  autoconf-archive gnu-standards autoconf-doc dh-make gettext-doc
  libasprintf-dev libgettextpo-dev groff libcairo2-doc colord cups-common
  libglib2.0-doc gvfs libgtk-3-doc libice-doc icu-doc liblcms2-utils
  libtool-doc imagemagick libpango1.0-doc librsvg2-bin libsm-doc gfortran
  | fortran95-compiler gcj-jdk libxcb-doc libxext-doc less www-browser
  libmail-box-perl
The following NEW packages will be installed:
  adwaita-icon-theme at-spi2-core autoconf automake autopoint autotools-dev
  dconf-gsettings-backend dconf-service debhelper dh-autoreconf
  dh-strip-nondeterminism fontconfig fontconfig-config fonts-dejavu-core
  gettext gettext-base gir1.2-atk-1.0 gir1.2-atspi-2.0 gir1.2-freedesktop
  gir1.2-gdkpixbuf-2.0 gir1.2-glib-2.0 gir1.2-gtk-3.0 gir1.2-pango-1.0
  glib-networking glib-networking-common glib-networking-services groff-base
  gsettings-desktop-schemas gtk-update-icon-cache hicolor-icon-theme
  icu-devtools intltool-debian libarchive-zip-perl libatk-bridge2.0-0
  libatk-bridge2.0-dev libatk1.0-0 libatk1.0-data libatk1.0-dev libatspi2.0-0
  libatspi2.0-dev libcairo-gobject2 libcairo-script-interpreter2 libcairo2
  libcairo2-dev libcolord2 libcroco3 libcups2 libdatrie1 libdbus-1-dev
  libdconf1 libdrm-amdgpu1 libdrm-dev libdrm-etnaviv1 libdrm-exynos1
  libdrm-freedreno1 libdrm-nouveau2 libdrm-omap1 libdrm-radeon1 libdrm-tegra0
  libdrm2 libegl1-mesa libegl1-mesa-dev libepoxy-dev libepoxy0 libexpat1-dev
  libfile-stripnondeterminism-perl libfontconfig1 libfontconfig1-dev
  libfreetype6 libfreetype6-dev libgbm1 libgdk-pixbuf2.0-0
  libgdk-pixbuf2.0-common libgdk-pixbuf2.0-dev libgirepository-1.0-1
  libglib2.0-bin libglib2.0-data libglib2.0-dev libgraphite2-3
  libgraphite2-dev libgtk-3-0 libgtk-3-bin libgtk-3-common libgtk-3-dev
  libharfbuzz-dev libharfbuzz-gobject0 libharfbuzz-icu0 libharfbuzz0b
  libice-dev libice6 libicu-dev libjbig0 libjpeg62-turbo libjson-glib-1.0-0
  libjson-glib-1.0-common liblcms2-2 libltdl-dev libltdl7 liblzo2-2
  libmail-sendmail-perl libpango-1.0-0 libpango1.0-dev libpangocairo-1.0-0
  libpangoft2-1.0-0 libpangoxft-1.0-0 libpcre16-3 libpcre3-dev libpcre32-3
  libpcrecpp0v5 libpixman-1-0 libpixman-1-dev libpng-dev libpng-tools
  libpng16-16 libproxy1v5 libpthread-stubs0-dev librest-0.7-0 librsvg2-2
  librsvg2-common libsm-dev libsm6 libsoup-gnome2.4-1 libsoup2.4-1
  libsys-hostname-long-perl libthai-data libthai0 libtiff5 libtimedate-perl
  libtool libwayland-bin libwayland-client0 libwayland-cursor0 libwayland-dev
  libwayland-egl1-mesa libwayland-server0 libx11-6 libx11-data libx11-dev
  libx11-doc libx11-xcb-dev libx11-xcb1 libxau-dev libxau6 libxcb-dri2-0
  libxcb-dri2-0-dev libxcb-dri3-0 libxcb-dri3-dev libxcb-glx0 libxcb-glx0-dev
  libxcb-present-dev libxcb-present0 libxcb-randr0 libxcb-randr0-dev
  libxcb-render0 libxcb-render0-dev libxcb-shape0 libxcb-shape0-dev
  libxcb-shm0 libxcb-shm0-dev libxcb-sync-dev libxcb-sync1 libxcb-xfixes0
  libxcb-xfixes0-dev libxcb1 libxcb1-dev libxcomposite-dev libxcomposite1
  libxcursor-dev libxcursor1 libxdamage-dev libxdamage1 libxdmcp-dev libxdmcp6
  libxext-dev libxext6 libxfixes-dev libxfixes3 libxft-dev libxft2 libxi-dev
  libxi6 libxinerama-dev libxinerama1 libxkbcommon-dev libxkbcommon0
  libxrandr-dev libxrandr2 libxrender-dev libxrender1 libxshmfence-dev
  libxshmfence1 libxtst-dev libxtst6 libxxf86vm-dev libxxf86vm1 man-db
  pkg-config po-debconf shared-mime-info wayland-protocols x11-common
  x11proto-composite-dev x11proto-core-dev x11proto-damage-dev
  x11proto-dri2-dev x11proto-fixes-dev x11proto-gl-dev x11proto-input-dev
  x11proto-kb-dev x11proto-randr-dev x11proto-record-dev x11proto-render-dev
  x11proto-xext-dev x11proto-xf86vidmode-dev x11proto-xinerama-dev xkb-data
  xorg-sgml-doctools xtrans-dev zlib1g-dev

apt-get install libssl-dev
OUTPUT:

The following additional packages will be installed:
  libssl-doc
The following NEW packages will be installed:
  libssl-dev libssl-doc

apt-get install xinit
OUTPUT:

The following additional packages will be installed:
  keyboard-configuration libevdev2 libfontenc1 libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libgudev-1.0-0 libinput-bin libinput10 libllvm4.0 libmtdev1 libpam-systemd libpciaccess0 libsensors4
  libtxc-dxtn-s2tc libutempter0 libwacom-bin libwacom-common libwacom2 libxaw7 libxfont1 libxfont2 libxkbfile1 libxmu6 libxmuu1 libxpm4 libxt6 libxv1 libxxf86dga1 x11-utils x11-xkb-utils xauth xbitmaps
  xfonts-base xfonts-encodings xfonts-utils xserver-common xserver-xorg xserver-xorg-core xserver-xorg-input-all xserver-xorg-input-libinput xserver-xorg-input-wacom xserver-xorg-legacy xserver-xorg-video-all
  xserver-xorg-video-amdgpu xserver-xorg-video-ati xserver-xorg-video-fbdev xserver-xorg-video-nouveau xserver-xorg-video-radeon xserver-xorg-video-vesa xterm
Suggested packages:
  pciutils lm-sensors mesa-utils xfonts-100dpi | xfonts-75dpi xfonts-scalable xinput firmware-amd-graphics xserver-xorg-video-r128 xserver-xorg-video-mach64 xfonts-cyrillic
The following NEW packages will be installed:
  keyboard-configuration libevdev2 libfontenc1 libgl1-mesa-dri libgl1-mesa-glx libglapi-mesa libgudev-1.0-0 libinput-bin libinput10 libllvm4.0 libmtdev1 libpam-systemd libpciaccess0 libsensors4
  libtxc-dxtn-s2tc libutempter0 libwacom-bin libwacom-common libwacom2 libxaw7 libxfont1 libxfont2 libxkbfile1 libxmu6 libxmuu1 libxpm4 libxt6 libxv1 libxxf86dga1 x11-utils x11-xkb-utils xauth xbitmaps
  xfonts-base xfonts-encodings xfonts-utils xinit xserver-common xserver-xorg xserver-xorg-core xserver-xorg-input-all xserver-xorg-input-libinput xserver-xorg-input-wacom xserver-xorg-legacy
  xserver-xorg-video-all xserver-xorg-video-amdgpu xserver-xorg-video-ati xserver-xorg-video-fbdev xserver-xorg-video-nouveau xserver-xorg-video-radeon xserver-xorg-video-vesa xterm


//Disable uboot loading capes - didn't get a chance to look into loading capes with this method, so stick with the old way
//Comment the following line:
enable_uboot_overlays=1
mv ft6236.ko /lib/modules/$(uname -r)/extra/ 

//Add driver for x server with lcd
cd /opt/scripts/tools
git pull
cd /opt/scripts/tools/graphics/
./ti-tilcdc.sh




8/21/17 Setting up IOT image from beagleboard.org releases (4.4.54-ti-r93 #1 SMP Fri Mar 17 13:08:22 UTC 2017 armv7l)
apt-get update
apt-get upgrade
apt-get install libgtk-3-dev
apt-get install libssl-dev
apt-get install xinit
apt-get install exfat-utils exfat-fuse
//Add touch driver: compile the file ft6236.ko on Beaglebone
apt-get install linux-headers-4.4.54-ti-r93
cd touchcontroller
make
//Move to extra module directory:
mv ft6236.ko /lib/modules/$(uname -r)/extra/
//First ensure that the latest device tree compiler is installed:
git clone https://github.com/beagleboard/bb.org-overlays
cd ./bb.org-overlays
./dtc-overlay.sh
./install.sh
//Compile the I2C touch device tree file BB-I2C1-Touch.dts and move to /lib/firmware:
dtc -O dtb -o BB-I2C1-Touch-00A0.dtbo -b 0 -@ BB-I2C1-Touch.dts
mv BB-I2C1-Touch-00A0.dtbo /lib/firmware
//Include the name in /etc/default/capemgr:
nano /etc/default/capemgr
//Change the last line to
CAPE=BB-I2C1-Touch
//Add the overlays for SPI0 for display and I2C1 for touch:
nano /boot/uEnv.txt
//Add the line
cape_enable=bone_capemgr.enable_partno=BB-SPIDEV0,BB-I2C1-Touch,BB-UART4
//Uncomment the line
dtb=am335x-boneblack-emmc-overlay.dtb
//clone hemapp repo
git clone https://gitlab.com/microgridenergymanager/hemapp.git
